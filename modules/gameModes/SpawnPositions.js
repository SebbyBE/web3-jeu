exports.default = function(width, height) {
	return [
		[{
			x: 24,
			y: 232
		}, {
			x: 24,
			y: 232
		}],
		[{
			x: 424,
			y: 232
		}, {
			x: 424,
			y: 232
		}]
	]
}
exports.small = function(width, height) {
	return [
		[{
				x: 24,
				y: 24
			},
			{
				x: 24,
				y: 40
			},
			{
				x: 40,
				y: 24
			},
			{
				x: 40,
				y: 40
			}
		],
		[{
				x: 16 * (width - 2) + 8,
				y: 16 * (height - 2) + 8
			},
			{
				x: 16 * (width - 2) + 8,
				y: 16 * (height - 3) + 8
			},
			{
				x: 16 * (width - 3) + 8,
				y: 16 * (height - 2) + 8
			},
			{
				x: 16 * (width - 3) + 8,
				y: 16 * (height - 3) + 8
			}
		]
	]
}
exports.medium = function(width, height) {
	return [
		[{
				x: 24,
				y: 24
			},
			{
				x: 24,
				y: 40
			},
			{
				x: 40,
				y: 24
			},
			{
				x: 40,
				y: 40
			},
			{
				x: 24,
				y: 56
			},
			{
				x: 40,
				y: 56
			},
			{
				x: 56,
				y: 24
			},
			{
				x: 56,
				y: 40
			},
			{
				x: 56,
				y: 56
			}
		],
		[{
				x: 16 * (width - 2) + 8,
				y: 16 * (height - 2) + 8
			},
			{
				x: 16 * (width - 2) + 8,
				y: 16 * (height - 3) + 8
			},
			{
				x: 16 * (width - 3) + 8,
				y: 16 * (height - 2) + 8
			},
			{
				x: 16 * (width - 3) + 8,
				y: 16 * (height - 3) + 8
			},
			{
				x: 16 * (width - 2) + 8,
				y: 16 * (height - 4) + 8
			},
			{
				x: 16 * (width - 3) + 8,
				y: 16 * (height - 4) + 8
			},
			{
				x: 16 * (width - 4) + 8,
				y: 16 * (height - 2) + 8
			},
			{
				x: 16 * (width - 4) + 8,
				y: 16 * (height - 3) + 8
			},
			{
				x: 16 * (width - 4) + 8,
				y: 16 * (height - 4) + 8
			}
		]
	]
}
exports.large = function(width, height) {
	return [
		[{
				x: 24,
				y: 24
			},
			{
				x: 24,
				y: 40
			},
			{
				x: 40,
				y: 24
			},
			{
				x: 40,
				y: 40
			},
			{
				x: 24,
				y: 56
			},
			{
				x: 40,
				y: 56
			},
			{
				x: 56,
				y: 24
			},
			{
				x: 56,
				y: 40
			},
			{
				x: 56,
				y: 56
			},
			{
				x: 72,
				y: 24
			},
			{
				x: 72,
				y: 40
			},
			{
				x: 72,
				y: 56
			},
			{
				x: 72,
				y: 72
			},
			{
				x: 24,
				y: 72
			},
			{
				x: 40,
				y: 72
			},
			{
				x: 56,
				y: 72
			}
		],
		[{
				x: 16 * (width - 2) + 8,
				y: 16 * (height - 2) + 8
			},
			{
				x: 16 * (width - 2) + 8,
				y: 16 * (height - 3) + 8
			},
			{
				x: 16 * (width - 3) + 8,
				y: 16 * (height - 2) + 8
			},
			{
				x: 16 * (width - 3) + 8,
				y: 16 * (height - 3) + 8
			},
			{
				x: 16 * (width - 2) + 8,
				y: 16 * (height - 4) + 8
			},
			{
				x: 16 * (width - 3) + 8,
				y: 16 * (height - 4) + 8
			},
			{
				x: 16 * (width - 4) + 8,
				y: 16 * (height - 2) + 8
			},
			{
				x: 16 * (width - 4) + 8,
				y: 16 * (height - 3) + 8
			},
			{
				x: 16 * (width - 4) + 8,
				y: 16 * (height - 4) + 8
			},
			{
				x: 16 * (width - 2) + 8,
				y: 16 * (height - 5) + 8
			},
			{
				x: 16 * (width - 3) + 8,
				y: 16 * (height - 5) + 8
			},
			{
				x: 16 * (width - 4) + 8,
				y: 16 * (height - 5) + 8
			},
			{
				x: 16 * (width - 5) + 8,
				y: 16 * (height - 2) + 8
			},
			{
				x: 16 * (width - 5) + 8,
				y: 16 * (height - 3) + 8
			},
			{
				x: 16 * (width - 5) + 8,
				y: 16 * (height - 4) + 8
			},
			{
				x: 16 * (width - 5) + 8,
				y: 16 * (height - 5) + 8
			}
		]
	]
}
